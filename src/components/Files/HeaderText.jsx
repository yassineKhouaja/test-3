import React from "react";

const HeaderText = ({ content }) => {
  const { icon, bgColor, tittle } = content;
  return (
    <div className="header-text">
      {icon && (
        <div className={`icon-container ${bgColor}`}>
          <img src={icon} alt="icon" className="icon-img" />
        </div>
      )}
      <h2>{tittle}</h2>
    </div>
  );
};

export default HeaderText;
