import React from "react";

const Icon = ({ icon, clickHandler, bgIcon }) => {
  return (
    <div className="item-box-icon">
      <div className={`icon-action ${bgIcon}`} onClick={clickHandler}>
        <img src={icon} alt="" />
      </div>
    </div>
  );
};

export default Icon;
