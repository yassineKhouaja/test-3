import React from "react";

const ListHeader = ({ actions, deleted, dateAt }) => {
  return (
    <li className="item head">
      <div className="item-box">
        <span>Name</span>
      </div>
      <div className="item-box">
        <span>Date {`${dateAt.slice(0, -2)}`} </span>
      </div>
      <div className="item-box item-box--small">
        <span>Size</span>
      </div>
      {(actions || deleted) && (
        <div className="item-box item-box--small">
          <span>Actions</span>
        </div>
      )}
    </li>
  );
};

export default ListHeader;
