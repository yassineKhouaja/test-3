import React from "react";

const Markup = ({ starred, archived }) => {
  return (
    <div>
      <span
        className={`icon-markup ${starred ? "icon-markup--yellow" : ""}`}
      ></span>
      <span
        className={`icon-markup second-markup ${
          archived ? "icon-markup--red" : ""
        }`}
      ></span>
    </div>
  );
};

export default Markup;
