import Star from "../../assets/icons/Star.svg";
import Box from "../../assets/icons/Box.svg";
import Delete from "../../assets/icons/Delete.svg";
import bytesToSize from "../../utils/bytesToSize";
import formatDate, { formatDateArray } from "../../utils/formatDate";
import pickIcon, { pickIconTexte } from "../../utils/pickIcon";
import { useDispatch } from "react-redux";
import { addArchived, addStarred } from "../../store/slices/files";
import { Fragment, useState } from "react";
import ListHeader from "./ListHeader";
import HeaderText from "./HeaderText";
import Markup from "./Markup";
import Icon from "./Icon";
const Files = ({ data, params }) => {
  const { tittle, bgColor, icon, markup, actions, deleted, filesCount, type } =
    params;
  const dispatch = useDispatch();
  const [sortBy, setSortBy] = useState(type);
  const dateFiles = formatDateArray(data);
  const sortedFiles = dateFiles
    .sort((a, b) => {
      if (sortBy === "name") return a.name.localeCompare(b.name);
      return b[sortBy] - a[sortBy];
    })
    .slice(0, filesCount ? 5 : undefined);
  return (
    <div className="all-files">
      <div className="all-files-header">
        <HeaderText content={{ icon, bgColor, tittle }} />
        {(actions || deleted) && (
          <div className="header-select">
            <select
              id="select-sort"
              name="select-sort"
              onChange={(e) => setSortBy(e.target.value)}
            >
              <option value="">Order By</option>
              <option value="name">By Name </option>
              <option value={type}>By Date</option>
              <option value="size">By Size</option>
            </select>
          </div>
        )}
      </div>
      <ul className="list">
        <ListHeader actions={actions} deleted={deleted} dateAt={type} />
        {sortedFiles.map((file) => (
          <li key={file.id} className="item">
            <div className="item-box">
              <div className="item-box-img">
                <img
                  src={pickIcon(file.type)}
                  alt=""
                  className={`icon-${pickIconTexte(file.type)}`}
                />
                {markup && (
                  <Markup starred={file.isStarred} archived={file.isArchived} />
                )}
              </div>
              <span>{`${file.name}.${file.type}`}</span>
            </div>
            <div className="item-box">
              <span>{formatDate(file[type])}</span>
            </div>
            <div className="item-box item-box--small">
              <span>{bytesToSize(file.size)}</span>
            </div>
            {(actions || deleted) && (
              <div className="item-box item-box--small">
                {actions && (
                  <Fragment>
                    <Icon
                      icon={Star}
                      clickHandler={() => dispatch(addStarred(file.id))}
                      bgIcon="favorite"
                    />
                    <Icon
                      icon={Box}
                      clickHandler={() => dispatch(addArchived(file.id))}
                      bgIcon="archive"
                    />
                  </Fragment>
                )}
                {deleted && (
                  <Icon
                    icon={Delete}
                    clickHandler={
                      type === "starredAt"
                        ? () => dispatch(addStarred(file.id))
                        : () => dispatch(addArchived(file.id))
                    }
                    bgIcon="delete"
                  />
                )}
              </div>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};
export default Files;
