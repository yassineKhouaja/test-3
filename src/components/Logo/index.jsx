import { Link } from "react-router-dom";
import logo from "../../assets/images/logo.svg";

const Logo = () => {
  return (
    <Link to="/">
      <div className="logo header-logo">
        <img src={logo} alt="logo" />
      </div>
    </Link>
  );
};

export default Logo;
