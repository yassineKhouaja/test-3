const maxStorage = { file: 104857600, image: 1073741824, video: 5368709120 };

export default maxStorage;
