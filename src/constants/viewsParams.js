import Box from "../assets/icons/Box.svg";
import FileStructure from "../assets/icons/FileStructure.svg";
import Star from "../assets/icons/Star.svg";
import HomeMinimal from "../assets/icons/HomeMinimal.svg";

const viewParams = [
  {
    tittle: "Recent Files",
    bgColor: null,
    icon: null,
    markup: false,
    actions: false,
    deleted: false,
    filesCount: 5,
    type: "createdAt",
  },
  {
    tittle: "All Files",
    bgColor: "all",
    icon: FileStructure,
    markup: true,
    actions: true,
    deleted: false,
    filesCount: null,
    type: "createdAt",
  },
  {
    tittle: "Starred Files",
    bgColor: "starred",
    icon: Star,
    markup: false,
    actions: false,
    deleted: true,
    filesCount: null,
    type: "starredAt",
  },
  {
    tittle: "Archived Files",
    bgColor: "archived",
    icon: Box,
    markup: false,
    actions: false,
    deleted: true,
    filesCount: null,
    type: "archivedAt",
  },
];

export const SidebarLinkList = [
  {
    icon: HomeMinimal,
    tittle: "Homes",
    path: "/",
  },
  {
    icon: FileStructure,
    tittle: "All Files",
    path: "/all-files",
  },
  {
    icon: Star,
    tittle: "Starred",
    path: "/starred",
  },
  {
    icon: Box,
    tittle: "Archived",
    path: "/archived",
  },
];

export default viewParams;
