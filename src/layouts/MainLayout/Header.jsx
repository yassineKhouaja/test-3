import AchrefPhto from "../../assets/images/user.png";
import Settings from "../../assets/icons/Settings.svg";
import Search from "../../assets/icons/Search.svg";
import NotificationBell from "../../assets/icons/NotificationBell.svg";
import Logo from "../../components/Logo";
const Header = ({ clickHandler }) => {
  const user = { userName: "Achref", userPhoto: AchrefPhto };
  return (
    <header className="header">
      <form action="#" className="search">
        <button className="search__button">
          <img src={Search} alt="" className="search__img" />
        </button>
        <input type="text" className="search__input" placeholder="Search" />
      </form>
      <nav className="user-nav">
        <Logo />
        <div
          className="user-nav__icon-box user-nav__icon-box--hamburger"
          onClick={() => clickHandler((prev) => !prev)}
        >
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div className="user-nav__icon-box">
          <img src={Settings} alt="" className="user-nav__icon-img" />
        </div>
        <div className="user-nav__icon-box">
          <img src={NotificationBell} alt="" className="user-nav__icon-img" />
          <span className="user-nav__notification">18</span>
        </div>
        <div className="user-nav__user">
          <span className="user-nav__user-name">{user.userName}</span>
          <img
            src={user.userPhoto}
            alt="user"
            className="user-nav__user-photo"
          />
        </div>
      </nav>
    </header>
  );
};

export default Header;
