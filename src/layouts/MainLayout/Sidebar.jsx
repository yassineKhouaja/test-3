import Upload from "../../assets/icons/Upload.svg";
import Logo from "../../components/Logo";
import { useDispatch, useSelector } from "react-redux";
import { addFiles, selectFiles } from "../../store/slices/files";
import { nanoid } from "nanoid";
import dateNow from "../../utils/dateNow";
import sizePourcentage from "../../utils/sizePourcentage";
import maxStorage from "../../constants/maxStorage";
import SidebarLink from "./SidebarLink";
import { SidebarLinkList } from "../../constants/viewsParams";
import displayError from "../../utils/dispalayError";

const Sidebar = ({ show, clickHandler }) => {
  const filesList = useSelector(selectFiles);
  const dispatch = useDispatch();

  const uploadHandler = (e) => {
    const files = [];
    const fileList = e.target.files;
    for (let index = 0; index < fileList.length; index++) {
      const element = fileList[index];
      files.push({
        id: nanoid(10),
        name: element.name.slice(0, -4),
        type: element.name.slice(-3).toLowerCase(),
        size: element.size,
        createdAt: dateNow(),
        isStarred: false,
        starredAt: null,
        isArchived: false,
        archivedAt: null,
      });
    }
    const availibaleSpace = sizePourcentage(filesList);
    const fileListNewFiles = [...filesList, ...files];
    const dataSize = sizePourcentage(fileListNewFiles);
    if (
      dataSize[0].image <= maxStorage.image &&
      dataSize[0].file <= maxStorage.file &&
      dataSize[0].video <= maxStorage.video
    ) {
      dispatch(addFiles(files));
    } else {
      displayError(maxStorage, availibaleSpace);
    }
  };
  return (
    <nav className={`sidebar ${show ? "show" : ""}`}>
      <div className="close" onClick={() => clickHandler(false)}>
        {" "}
        &times;
      </div>
      <input
        type="file"
        name="inputFile"
        id="inputFile"
        accept=".jpg, .gif, .png, .txt, .pdf, .doc, .mp4,  .avi,  .mov"
        multiple
        onChange={uploadHandler}
        className="sidebar-input"
      />
      <Logo />
      <button
        className="sidebar__btn"
        onClick={() => {
          document.querySelector("#inputFile").click();
          clickHandler(false);
        }}
      >
        <img src={Upload} alt="logo" />
        Upload
      </button>
      <ul className="side-nav">
        {SidebarLinkList.map((link) => (
          <SidebarLink
            icon={link.icon}
            tittle={link.tittle}
            path={link.path}
            key={link.tittle}
            clickHandler={clickHandler}
          />
        ))}
      </ul>
    </nav>
  );
};

export default Sidebar;
