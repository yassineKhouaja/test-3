import { NavLink } from "react-router-dom";

const SidebarLink = ({ icon, tittle, path, clickHandler }) => {
  return (
    <li className="side-nav__item" onClick={() => clickHandler(false)}>
      <NavLink to={path} className="side-nav__link">
        <img src={icon} className="side-nav__icon" alt="" />
        <span>{tittle}</span>
      </NavLink>
    </li>
  );
};

export default SidebarLink;
