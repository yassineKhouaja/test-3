import Sidebar from "./Sidebar";
import Header from "./Header";
import { useState } from "react";

const MainLayout = ({ children }) => {
  const [show, setShow] = useState(false);
  return (
    <div className="main-layout">
      <Sidebar show={show} clickHandler={setShow} />
      <div className="container">
        <Header clickHandler={setShow} />
        {children}
      </div>
    </div>
  );
};

export default MainLayout;
