import { createSlice } from "@reduxjs/toolkit";
import FILES from "../../constants/files";
import dateNow from "../../utils/dateNow";

const initialState = {
  files: FILES,
  value: 0,
};

export const filesSlice = createSlice({
  name: "files",
  initialState,
  reducers: {
    addFiles: (state, action) => {
      state.files = [...action.payload, ...state.files];
    },

    addArchived: (state, action) => {
      state.files = state.files.map((file) =>
        file.id === action.payload
          ? {
              ...file,
              isArchived: !file.isArchived,
              archivedAt: file.isArchived ? null : dateNow(),
            }
          : file
      );
    },

    addStarred: (state, action) => {
      state.files = state.files.map((file) =>
        file.id === action.payload
          ? {
              ...file,
              isStarred: !file.isStarred,
              starredAt: file.isStarred ? null : dateNow(),
            }
          : file
      );
    },
  },
});

export const { addFiles, addArchived, addStarred } = filesSlice.actions;

export const selectFiles = (state) => state.files.files;

export default filesSlice.reducer;
