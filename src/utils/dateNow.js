const dateNow = () => {
  const dataUpload = new Date().toISOString(" ").slice(0, 19).replace("T", " ");
  return dataUpload;
};

export default dateNow;
