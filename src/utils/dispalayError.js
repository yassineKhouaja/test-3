import { toast } from "react-toastify";
import bytesToSize from "./bytesToSize";

const displayError = (maxStorage, availibaleSpace) => {
  toast.error(
    <div>
      <p>There is insufficient space to complete the operation !</p>
      <br />
      <p>Availibale space: </p>
      <p>
        {`Files: ${bytesToSize(maxStorage.file - availibaleSpace[0].file)}`}
      </p>
      <p>
        {`Images: ${bytesToSize(maxStorage.image - availibaleSpace[0].image)}`}
      </p>
      <p>
        {`Videos: ${bytesToSize(maxStorage.video - availibaleSpace[0].video)}`}
      </p>
    </div>,

    {
      theme: "colored",
    },
    {
      position: "top-center",
      autoClose: false,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }
  );
};
export default displayError;
