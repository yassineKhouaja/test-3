const formatDate = (date) => {
  if (!date) return;

  var mydate = new Date(date);
  const options = {
    month: "long",
    day: "numeric",
    year: "numeric",
  };
  return mydate.toLocaleDateString("en-EN", options);
};

export const formatDateArray = (data) =>
  data.map((file) => {
    const created = new Date(file.createdAt);
    const starred = new Date(file.starredAt);
    const archived = new Date(file.archivedAt);
    return {
      ...file,
      createdAt: created,
      starredAt: starred,
      archivedAt: archived,
    };
  });

export default formatDate;
