import Video from "../assets/icons/Video.svg";
import File from "../assets/icons/File.svg";
import Image from "../assets/icons/Image.svg";

const pickIcon = (format) => {
  if (!format) return;

  switch (format) {
    case "pdf":
    case "txt":
    case "doc":
    case "file":
      return File;
    case "gif":
    case "png":
    case "jpg":
    case "image":
      return Image;
    case "avi":
    case "mp4":
    case "mov":
    case "video":
      return Video;
    default:
      break;
  }
};

export const pickIconTexte = (format) => {
  if (!format) return;

  switch (format) {
    case "pdf":
    case "txt":
    case "doc":
    case "file":
      return "file";
    case "gif":
    case "png":
    case "jpg":
    case "image":
      return "image";
    case "avi":
    case "mp4":
    case "mov":
    case "video":
      return "video";
    default:
      break;
  }
};

export default pickIcon;
