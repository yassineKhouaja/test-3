import maxStorage from "../constants/maxStorage";
import { pickIconTexte } from "./pickIcon";

export const sizePourcentage = (data) => {
  const fileSize = { image: 0, video: 0, file: 0 };

  for (let index = 0; index < data.length; index++) {
    if (pickIconTexte(data[index].type) === "file")
      fileSize.file += parseInt(data[index].size, 10);

    if (pickIconTexte(data[index].type) === "image")
      fileSize.image += parseInt(data[index].size, 10);

    if (pickIconTexte(data[index].type) === "video")
      fileSize.video += parseInt(data[index].size, 10);
  }
  const filePourcentage = { image: 0, video: 0, file: 0 };
  filePourcentage.image = ((100 * fileSize.image) / maxStorage.image).toFixed(
    0
  );
  filePourcentage.video = ((100 * fileSize.video) / maxStorage.video).toFixed(
    0
  );
  filePourcentage.file = ((100 * fileSize.file) / maxStorage.file).toFixed(0);

  return [fileSize, filePourcentage];
};

export default sizePourcentage;
