import { useSelector } from "react-redux";
import Files from "../../components/Files";
import viewParams from "../../constants/viewsParams";
import { selectFiles } from "../../store/slices/files";

const AllFiles = () => {
  const filesList = useSelector(selectFiles);

  return <Files data={filesList} params={viewParams[1]} />;
};

export default AllFiles;
