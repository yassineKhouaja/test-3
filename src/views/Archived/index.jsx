import Files from "../../components/Files";
import { selectFiles } from "../../store/slices/files";
import { useSelector } from "react-redux";
import viewParams from "../../constants/viewsParams";
const Archived = () => {
  const filesList = useSelector(selectFiles);

  const archivedFiles = filesList.filter((file) => file.isArchived);

  return <Files data={archivedFiles} params={viewParams[3]} />;
};

export default Archived;
