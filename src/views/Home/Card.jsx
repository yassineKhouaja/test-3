import React from "react";
import maxStorage from "../../constants/maxStorage";
import bytesToSize from "../../utils/bytesToSize";
import pickIcon from "../../utils/pickIcon";

const Card = ({ cardType, tittle, pourcentage, value }) => {
  return (
    <div className="card-files">
      <div className={`icon-box ${cardType}`}>
        <img src={pickIcon(cardType)} alt="" />
      </div>
      <h3>{tittle}</h3>
      <div className={cardType}></div>
      <progress max="100" value={pourcentage} className={cardType}></progress>
      <p>
        <span className="card-percentage">{pourcentage} %</span>
        <span className="card-informations">
          {bytesToSize(value)} of {bytesToSize(maxStorage[cardType])} Used
        </span>
      </p>
    </div>
  );
};

export default Card;
