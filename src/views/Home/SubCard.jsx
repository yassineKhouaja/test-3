import React from "react";
import { NavLink } from "react-router-dom";

const SubCard = ({ icon, title, bgColor, link }) => {
  return (
    <div className="sub-card">
      <div className={`icon-box ${bgColor}`}>
        <img src={icon} alt="icon" />
      </div>
      <div className="sub-card-text">
        <h3>{title}</h3>
        <NavLink to={`/${link}`} className="sub-card-link">
          Go to view
        </NavLink>
      </div>
    </div>
  );
};

export default SubCard;
