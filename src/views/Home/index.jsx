import Star from "../../assets/icons/Star.svg";
import Box from "../../assets/icons/Box.svg";
import Card from "./Card";
import Files from "../../components/Files";
import SubCard from "./SubCard";
import sizePourcentage from "../../utils/sizePourcentage";
import { useSelector } from "react-redux";
import { selectFiles } from "../../store/slices/files";
import maxStorageData from "../../constants/maxStorage";
import viewParams from "../../constants/viewsParams";

const Home = () => {
  const maxStorage = maxStorageData;
  const filesList = useSelector(selectFiles);
  const dataSize = sizePourcentage(filesList, maxStorage);
  const archivedFiles = filesList.filter((file) => file.isArchived).length;
  const starredFiles = filesList.filter((file) => file.isStarred).length;
  const CardList = [
    { type: "file", tittle: "Documents" },
    { type: "image", tittle: "Images" },
    { type: "video", tittle: "Video" },
  ];
  const subCardList = [
    { icon: Star, title: starredFiles, bgColor: "starred", link: "starred" },
    { icon: Box, title: archivedFiles, bgColor: "archived", link: "archived" },
  ];
  return (
    <div className="home">
      <div className="card-list">
        {CardList.map((item) => (
          <Card
            cardType={item.type}
            tittle={item.tittle}
            pourcentage={dataSize[1][item.type]}
            value={dataSize[0][item.type]}
            key={item.tittle}
          />
        ))}
      </div>
      <div className="content">
        <div className="content-files">
          <Files data={filesList} params={viewParams[0]} />
        </div>

        <div className="aside">
          {subCardList.map((subCard) => (
            <SubCard
              icon={subCard.icon}
              title={`${subCard.title} Starred Files`}
              bgColor={subCard.bgColor}
              link={subCard.link}
              key={subCard.link}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
