import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <div className="not-found">
      <h1 className="not-found">
        <span>404</span> <br /> NOT FOUND
      </h1>
      <p>the resource requested could not be found on this server</p>
      <Link to="/" className="link">
        GO HOME &rarr;
      </Link>
    </div>
  );
};

export default NotFound;
