import Files from "../../components/Files";
import { useSelector } from "react-redux";
import { selectFiles } from "../../store/slices/files";
import viewParams from "../../constants/viewsParams";

const Starred = () => {
  const filesList = useSelector(selectFiles);

  const starredFiles = filesList.filter((file) => file.isStarred);
  return <Files data={starredFiles} params={viewParams[2]} />;
};

export default Starred;
